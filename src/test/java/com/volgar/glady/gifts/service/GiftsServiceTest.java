package com.volgar.glady.gifts.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.volgar.glady.gifts.infrastructure.dao.GiftsDao;
import com.volgar.glady.gifts.infrastructure.entity.Gift;
import com.volgar.glady.gifts.models.GiftCreation;

@SpringBootTest
public class GiftsServiceTest {
    
    private static final LocalDate DATE = LocalDate.of(2022, 10, 9);
    private static final GiftCreation GIFT = new GiftCreation("A super compagny", 123L, 1L);

    @Autowired
    private GiftsService giftsService;

    @MockBean
    private GiftsDao giftsDao;

    @Test
    public void givenDate_whenCreateGift_thenExpirationDateSet1YearLater() {
        LocalDate expetedExpirationDate = LocalDate.of(2023, 10, 8);

        giftsService.createGift(GIFT, DATE);
        Mockito.verify(giftsDao, times(1)).createGift(GIFT, expetedExpirationDate);
    }

    @Test
    public void givenDateNull_whenCreateGift_thenDoNothing() {
        giftsService.createGift(GIFT, null);
        Mockito.verify(giftsDao, times(0)).createGift(Mockito.any(), Mockito.any());
    }

    @Test
    public void givenGiftNull_whenCreateGift_thenDoNothing() {
        giftsService.createGift(null, DATE);
        Mockito.verify(giftsDao, times(0)).createGift(Mockito.any(), Mockito.any());
    }

    @Test
    public void givenGiftsWithTotalValue240_whenGetGiftsBalanceFromUser_thenReturn240() {
        Gift gift1 = new Gift();
        Gift gift2 = new Gift();
        gift1.setAmount(100L);
        gift2.setAmount(140L);
        List<Gift> gifts = List.of(gift1, gift2);
        Mockito.when(giftsDao.getGifts(Mockito.any(), Mockito.any())).thenReturn(gifts);
        assertEquals(240L, giftsService.getGiftsBalanceFromUser(1L, null));
    }
}
