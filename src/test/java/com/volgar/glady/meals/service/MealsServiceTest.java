package com.volgar.glady.meals.service;

import static org.mockito.Mockito.times;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.volgar.glady.meals.infrastructure.dao.MealDao;
import com.volgar.glady.meals.models.MealCreation;

@SpringBootTest
public class MealsServiceTest {

    private static final LocalDate DATE = LocalDate.of(2022, 10, 9);
    private static final MealCreation MEAL = new MealCreation("A super compagny", 123L, 1L);

    @Autowired
    private MealsService mealsService;

    @MockBean
    private MealDao mealDao;

    @Test
    public void givenDate_whenCreateMeal_thenExpirationDateSetLastDayOfFebuary() {
        LocalDate expetedExpirationDate = LocalDate.of(2023, 2, 28);

        mealsService.createMeal(MEAL, DATE);
        Mockito.verify(mealDao, times(1)).createMeal(MEAL, expetedExpirationDate);
    }

    @Test
    public void givenDateNull_whenCreateMeal_thenDoNothing() {
        mealsService.createMeal(MEAL, null);
        Mockito.verify(mealDao, times(0)).createMeal(Mockito.any(), Mockito.any());
    }

    @Test
    public void givenGiftNull_whenCreateMeal_thenDoNothing() {
        mealsService.createMeal(null, DATE);
        Mockito.verify(mealDao, times(0)).createMeal(Mockito.any(), Mockito.any());
    }
}
