package com.volgar.glady.users.infrastructure.dao;

import java.util.List;

import com.volgar.glady.users.infrastructure.entity.User;
import com.volgar.glady.users.models.UserCreation;

public interface UserDao {
     
    public List<User> getUsers();

    public void createUser(UserCreation UserCreation);

    public User getUser(Long userId);
}
