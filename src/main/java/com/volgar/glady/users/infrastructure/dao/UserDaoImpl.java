package com.volgar.glady.users.infrastructure.dao;

import static com.volgar.glady.common.infrastructure.ParentDao.getParentDao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.volgar.glady.users.infrastructure.entity.User;
import com.volgar.glady.users.models.UserCreation;

@Component
public class UserDaoImpl implements UserDao {

    @Override
    public List<User> getUsers() {
        String query = "Select user from " + User.class.getSimpleName() + " user";
        return getParentDao().list(query, null, User.class);
    }

    @Override
    public void createUser(UserCreation userCreation) {
        User user = new User();
        user.setFirstName(userCreation.getFirstName());
        user.setLastName(userCreation.getLastName());
        getParentDao().saveEntity(user);
    }

    @Override
    public User getUser(Long userId) {
        return getParentDao().getEntity(userId, User.class);
    }
}
