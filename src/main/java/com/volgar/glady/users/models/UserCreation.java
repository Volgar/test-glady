package com.volgar.glady.users.models;


public class UserCreation {
    
    private String firstName;

    private String lastName;

    public UserCreation(String firstname, String lastname) {
        this.firstName = firstname;
        this.lastName = lastname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    } 
}
