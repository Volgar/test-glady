package com.volgar.glady.users.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.volgar.glady.gifts.service.GiftsService;
import com.volgar.glady.meals.service.MealsService;
import com.volgar.glady.users.infrastructure.dao.UserDao;
import com.volgar.glady.users.infrastructure.entity.User;
import com.volgar.glady.users.models.UserCreation;

@Component
public class UserService {
    
    @Autowired
    private UserDao userDao;

    @Autowired
    private GiftsService giftsService;

    @Autowired
    private MealsService mealsService;

    public List<User> getUsers() {
        return userDao.getUsers();
    }

   public void createUser(UserCreation giftCreation) {
        userDao.createUser(giftCreation);
    }

    public User getUser(Long userId) {
        return userDao.getUser(userId);
    }

    public Long getUserBalance(Long userId, LocalDate now) {
        return giftsService.getGiftsBalanceFromUser(userId, now) + mealsService.getGiftsBalanceFromUser(userId, now);
    }
}
