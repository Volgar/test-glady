package com.volgar.glady.users.api;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.volgar.glady.users.infrastructure.entity.User;
import com.volgar.glady.users.models.UserCreation;
import com.volgar.glady.users.service.UserService;

@RestController
@RequestMapping("/api/v1/users")
public class UserApi {
    
    @Autowired
    UserService userService;

    @GetMapping("/")
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{userId}")
    public User getUser(@PathVariable("userId") Long userId) {
        return userService.getUser(userId);
    }

    @GetMapping("/{userId}/balance")
    public Long getUserBalance(@PathVariable("userId") Long userId) {
        return userService.getUserBalance(userId, LocalDate.now());
    }

    @PostMapping("/create")
    public void createUser(@RequestBody UserCreation userCreation) {
        userService.createUser(userCreation);
    }
}
