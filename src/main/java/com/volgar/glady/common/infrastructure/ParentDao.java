package com.volgar.glady.common.infrastructure;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.volgar.glady.users.infrastructure.entity.User;

public class ParentDao {
    
    private static ParentDao parentDao;

    private EntityManagerFactory entityManagerFactory;
    private EntityManager entityManager;

    private ParentDao() {   
        entityManagerFactory = Persistence.createEntityManagerFactory("connect");     
        entityManager = entityManagerFactory.createEntityManager();
    }
    
    public static ParentDao getParentDao() {
        if(parentDao == null) {
            parentDao = new ParentDao();
        }
        
        return parentDao;
    }

    public void saveEntity(Object entity) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(entity);
        tx.commit();
    }

    public <T> List<T> list(String query, Map<String, Object> params, Class<T> className) {
        Query localQuery = entityManager.createQuery(query, className);
        if (params != null) {
            params.entrySet().stream().forEach(param -> localQuery.setParameter(param.getKey(), param.getValue()));
        }
        System.out.println(localQuery.getParameters());
        return localQuery.getResultList();
    }

	public <T> T getEntity(Long userId, Class<T> className) {
		return entityManager.find(className, userId);
	}

    public <T> T getReference(Long id, Class<T> className) {
        return entityManager.getReference(className, id);
    }
}
