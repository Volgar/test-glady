package com.volgar.glady.meals.models;

public class MealCreation {

    private String compagny;

    private Long amount;

    private Long userId;

    public MealCreation(String compagny, Long amount, Long userId) {
        this.compagny = compagny;
        this.amount = amount;
        this.userId = userId;
    }

    public String getCompagny() {
        return compagny;
    }

    public Long getAmount() {
        return amount;
    }

    public Long getUserId() {
        return userId;
    }
}
