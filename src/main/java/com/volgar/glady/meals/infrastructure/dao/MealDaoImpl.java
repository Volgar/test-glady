package com.volgar.glady.meals.infrastructure.dao;

import static com.volgar.glady.common.infrastructure.ParentDao.getParentDao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.volgar.glady.meals.infrastructure.entity.Meal;
import com.volgar.glady.meals.models.MealCreation;
import com.volgar.glady.users.infrastructure.entity.User;

@Component
public class MealDaoImpl implements MealDao {

    @Override
    public List<Meal> getMeals(LocalDate dateExpiration, Optional<Long> userId) {
        Map<String, Object> params = new HashMap<>();
        String query = "Select meal from " + Meal.class.getSimpleName() + " meal where meal.expiration > :NOW";
        if (userId.isPresent()) {
            query += " AND meal.user.id = :USERID";
            params.put("USERID", userId.get());
        }
        params.put("NOW", dateExpiration);
        return getParentDao().list(query, params, Meal.class);
    }

    @Override
    public void createMeal(MealCreation mealCreation, LocalDate dateExpiration) {
        Meal meal = new Meal();
        User user = getParentDao().getReference(mealCreation.getUserId(), User.class);
        
        meal.setAmount(mealCreation.getAmount());
        meal.setCompagny(mealCreation.getCompagny());
        meal.setUser(user);
        meal.setExpiration(dateExpiration);
        getParentDao().saveEntity(meal);
    }
}
