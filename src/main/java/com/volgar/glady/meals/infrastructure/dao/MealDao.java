package com.volgar.glady.meals.infrastructure.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.volgar.glady.meals.infrastructure.entity.Meal;
import com.volgar.glady.meals.models.MealCreation;

public interface MealDao {
    public List<Meal> getMeals(LocalDate dateExpiration, Optional<Long> userId);

    public void createMeal(MealCreation mealCreation, LocalDate dateExpiration);
}
