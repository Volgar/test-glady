package com.volgar.glady.meals.service;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.volgar.glady.meals.infrastructure.dao.MealDao;
import com.volgar.glady.meals.infrastructure.entity.Meal;
import com.volgar.glady.meals.models.MealCreation;

@Component
public class MealsService {

    @Autowired
    private MealDao mealDao;

    public List<Meal> getMeals(LocalDate now) {
        return mealDao.getMeals(now, Optional.empty());
    }

    public void createMeal(MealCreation giftCreation, LocalDate now) {
        if (giftCreation == null || now == null) {
            return;
        }
        YearMonth mounth = YearMonth.of(now.getYear() + 1, Month.FEBRUARY);
        mealDao.createMeal(giftCreation, mounth.atEndOfMonth());
    }

    public Long getGiftsBalanceFromUser(Long userId, LocalDate now) {
        List<Meal> userMeals = mealDao.getMeals(now, Optional.of(userId));
        return userMeals.stream().mapToLong(meal -> meal.getAmount()).sum();
    }
}
