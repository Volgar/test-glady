package com.volgar.glady.meals.api;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.volgar.glady.meals.infrastructure.entity.Meal;
import com.volgar.glady.meals.models.MealCreation;
import com.volgar.glady.meals.service.MealsService;

@RestController
@RequestMapping("/api/v1/meals")
public class MealsApi {
    
    @Autowired
    private MealsService mealsService;

    @GetMapping("/")
    public List<Meal> getGifts() {
        return mealsService.getMeals(LocalDate.now());
    }

    @PostMapping("/create")
    public void createGift(@RequestBody MealCreation mealCreation) {
        mealsService.createMeal(mealCreation, LocalDate.now());
    }
}
