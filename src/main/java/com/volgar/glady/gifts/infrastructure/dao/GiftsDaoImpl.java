package com.volgar.glady.gifts.infrastructure.dao;

import static com.volgar.glady.common.infrastructure.ParentDao.getParentDao;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.volgar.glady.gifts.infrastructure.entity.Gift;
import com.volgar.glady.gifts.models.GiftCreation;
import com.volgar.glady.users.infrastructure.entity.User;

@Component
public class GiftsDaoImpl implements GiftsDao {

    @Override
    public List<Gift> getGifts(LocalDate dateExpiration, Optional<Long> userId) {
        Map<String, Object> params = new HashMap<>();
        String query = "Select gift from " + Gift.class.getSimpleName() + " gift where gift.expiration > :NOW";
        if (userId.isPresent()) {
            query += " AND gift.user.id = :USERID";
            params.put("USERID", userId.get());
        }
        params.put("NOW", dateExpiration);
        return getParentDao().list(query, params, Gift.class);
    }

    @Override
    public void createGift(GiftCreation giftCreation, LocalDate dateExpiration) {
        Gift gift = new Gift();
        User user = getParentDao().getReference(giftCreation.getUserId(), User.class);
        
        gift.setAmount(giftCreation.getAmount());
        gift.setCompagny(giftCreation.getCompagny());
        gift.setUser(user);
        gift.setExpiration(dateExpiration);
        getParentDao().saveEntity(gift);
    } 
}
