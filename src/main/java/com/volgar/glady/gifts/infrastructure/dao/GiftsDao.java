package com.volgar.glady.gifts.infrastructure.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import com.volgar.glady.gifts.infrastructure.entity.Gift;
import com.volgar.glady.gifts.models.GiftCreation;

public interface GiftsDao {
    public List<Gift> getGifts(LocalDate dateExpiration, Optional<Long> userId);

    public void createGift(GiftCreation giftCreation, LocalDate dateExpiration);
}
