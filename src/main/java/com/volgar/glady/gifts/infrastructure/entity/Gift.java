package com.volgar.glady.gifts.infrastructure.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.volgar.glady.users.infrastructure.entity.User;

@Entity
public class Gift {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long giftId;

    private Long amount;

    private String compagny;

    private LocalDate expiration;

    @ManyToOne(targetEntity=User.class)
    private User user;

    public Gift() {
        
    }

    public Long getGiftId() {
        return giftId;
    }

    public void setGiftId(Long giftId) {
        this.giftId = giftId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getCompagny() {
        return compagny;
    }

    public void setCompagny(String compagny) {
        this.compagny = compagny;
    }

    public LocalDate getExpiration() {
        return expiration;
    }

    public void setExpiration(LocalDate expiration) {
        this.expiration = expiration;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
