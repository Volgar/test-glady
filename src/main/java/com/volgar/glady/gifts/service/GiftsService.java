package com.volgar.glady.gifts.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.volgar.glady.gifts.infrastructure.dao.GiftsDao;
import com.volgar.glady.gifts.infrastructure.entity.Gift;
import com.volgar.glady.gifts.models.GiftCreation;

@Component
public class GiftsService {
    
    @Autowired
    private GiftsDao giftDao;

    public List<Gift> getGifts(LocalDate now) {
        return giftDao.getGifts(now, Optional.empty());
    }

    public void createGift(GiftCreation giftCreation, LocalDate now) {
        if (giftCreation == null || now == null) {
            return;
        }
        LocalDate expirationDate = now.plusYears(1).minusDays(1);
        giftDao.createGift(giftCreation, expirationDate);
    }

    public Long getGiftsBalanceFromUser(Long userId, LocalDate now) {
        List<Gift> userGifts = giftDao.getGifts(now, Optional.of(userId));
        return userGifts.stream().mapToLong(gift -> gift.getAmount()).sum();
    }
}
