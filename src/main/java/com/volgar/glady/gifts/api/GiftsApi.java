package com.volgar.glady.gifts.api;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.volgar.glady.gifts.infrastructure.entity.Gift;
import com.volgar.glady.gifts.models.GiftCreation;
import com.volgar.glady.gifts.service.GiftsService;

@RestController
@RequestMapping("/api/v1/gifts")
public class GiftsApi {

    @Autowired
    GiftsService giftsService;

    @GetMapping("/")
    public List<Gift> getGifts() {
        return giftsService.getGifts(LocalDate.now());
    }

    @PostMapping("/create")
    public void createGift(@RequestBody GiftCreation giftCreation) {
        giftsService.createGift(giftCreation, LocalDate.now());
    }
}