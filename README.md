## Test Glady


## [Subject](./subject.md)

## [Documentation de l'API](./Api.md)

## Installation

Pour pouvoir faire fonctionner le projet en local, il faut avoir d'installer
- [Java 17](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html)
- [mysql](https://dev.mysql.com/doc/mysql-getting-started/en/)

Toutes les informations de connexion a la base sont disponible dans le fichier `persistence.xml`.

## Utilisation

Installation du projet: `mvn install`

Lancement des tests unitaires: `mvn test`

Lancement de l'application: `mvn spring-boot:run`

## Description du projet

### Technique

L'exercice est à faire en Java. Pour sa simplicité à mettre en place, j'ai choisi le framework sprint-boot pour l'application.  
Pour profiter de l'exercice pour monter en compérence, j'ai choisie d'utiliser mysql que je n'avais jamais utilisé auparavant.  
Pour plus de simplicité de code, et parce que j'ai l'habitude de l'utiliser, j'ai choisi d'utiliser le framework Hibernate pour gérer les entitées.  

Pour ne pas passer trop de temps sur l'exercice, j'ai délibérément choisi de ne pas implémenter:
- Une doc swagger pour l'api
- Un systeme d'authentification 
- Des tests d'intégrations
- Une CI
- Un linter

Pour tester l'application, le plus simple est d'utiliser [postman](https://www.postman.com/) ou [insomnia](https://insomnia.rest/)

Hibernate est actuellement en mode `create`, ce qui efface toutes les données au démarage de l'application.  
Pour changer de configuration, il faut éditer le fichier `persistance.xml`, cf. [documentation hibernate](https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#configurations-hbmddl)

### Implémentation

Apres lecture du sujet, je me suis rendu compte qu'il pouvai y avoir 2 implémentations différentes:
- L'une avec une table qui gère tout les types de cadeaux reçu par un utilisateur, et avec une colonne en base pour différencier les types de cadeaux.  
- L'autre avec une séparation plus forte des 2 métiers, avec des tables différentes en base.

J'ai choisie la 2eme implémentation, meme si les api `Meals` et `Gifts` se resemble énormément. Cela permet d'avoir une vrai séparation entre les 2 domaines, et d'avoir la possibilité de les faire evoluer séparément.  

Comme il y a peu de métier dans cette exercice, il y a aussi peu de TU.

Pour pouvoir garder un historique des données, il n'y a pas de suppression physique des données, meme lorsque les bons sont expirés.

