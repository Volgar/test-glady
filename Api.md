## User

Retourne la liste de tous les utilisateurs  
`GET /api/v1/users/`

Retourne un utilisateur spéfifique via l'id  
`GET /api/v1/users/{userId}`  

Retourne la balance d'un utilisateur (inclue Gifts et Meals)  
`GET /api/v1/users/{userId}/balance`

Créer un utilisateur  
`POST /api/v1/users/create`  

Body: 
```JSON
{
    "firstname": "Jack",
    "lastname": "O'Neill"
}
```


## Gifts

Retourne la liste de tous les cadeaux   
`GET /api/v1/gifts/`

Créer un cadeau   
`POST /api/v1/gifts/create`

Body  
```JSON
{
    "compagny": "Google",
    "amount": 123,
    "userId": 1
}
```

## Meals

Retourne la liste de tous les bon pour repas  
`GET /api/v1/meals/`

Créer un bon pour des repas  
`POST /api/v1/meals/create`

Body  
```JSON
{
    "compagny": "Google",
    "amount": 123,
    "userId": 1
}
```